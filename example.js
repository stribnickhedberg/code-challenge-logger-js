function Logger(config) {
        this.availableLoggers = {};
        this.chosenLogger = '';
        this.defaultMessage = config.defaultMessage || "";

        this.addLogger = function(index, cb) {
                if (typeof cb == 'function') {
                        this.availableLoggers[index] = cb;
                        if (this.chosenLogger == '') {
                                this.chosenLogger = index;
                        }
                }
        }

        this.setDefaultLogger = function(index) {
                if (undefined !== this.availableLoggers[index]) {
                        this.chosenLogger = index;
                }
        }

        this.send = function(msg) {
                if (typeof msg === 'undefined' || msg.length == 0) {
                        msg = this.defaultMessage;
                }
                for (index in this.availableLoggers) {
                        this.availableLoggers[index].call(this, msg);
                }
        }

        this.sendToDefault = function(msg) {
                this.availableLoggers[this.chosenLogger].call(this, msg);
        }
}

var logger = new Logger({defaultMessage: "Hey"});

logger.addLogger('dw', function(msg) {
        document.write(msg);});

logger.addLogger('alert', function(msg) {
        alert(msg);});

logger.setDefaultLogger('alert2');

logger.send();

